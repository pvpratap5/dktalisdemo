package com.orange.dktalisapp.di.component

import android.app.Application
import com.orange.dktalisapp.DktalisApp
import com.orange.dktalisapp.di.builder.ActivityBuildersModule
import com.orange.dktalisapp.di.module.AppModule
import javax.inject.Singleton
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityBuildersModule::class])
interface AppComponent : AndroidInjector<DktalisApp> {

    override fun inject(app: DktalisApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

}
