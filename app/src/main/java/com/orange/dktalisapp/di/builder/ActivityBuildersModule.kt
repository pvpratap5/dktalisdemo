package com.orange.dktalisapp.di.builder

import com.orange.dktalisapp.ui.root.RootModule
import com.orange.dktalisapp.ui.root.RootScreen
import com.orange.dktalisapp.ui.splash.SplashModule
import com.orange.dktalisapp.ui.splash.SplashScreen
import com.orange.dktalisapp.ui.splash.SplashViewModel
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule{
    @ContributesAndroidInjector(modules = [SplashModule::class])
    internal abstract fun contributeSplashActivity(): SplashScreen

    @ContributesAndroidInjector(modules = [RootModule::class])
    internal abstract fun contributeRootActivity(): RootScreen
}
