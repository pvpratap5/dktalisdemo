package com.orange.dktalisapp.service

import com.orange.dktalisapp.ui.root.DataModels.ProfileData
import io.reactivex.Flowable
import retrofit2.http.GET

interface ProfileService {
    @GET("/api/0.4/?randomapi")
    open fun getProfileData(): Flowable<ProfileData>?
}