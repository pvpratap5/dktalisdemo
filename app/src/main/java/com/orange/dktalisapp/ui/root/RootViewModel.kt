package com.orange.dktalisapp.ui.root

import com.orange.dktalis.appbase.BaseViewModel
import com.orange.dktalisapp.service.ProfileService
import com.orange.dktalisapp.ui.root.DataModels.ProfileData
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET

class RootViewModel : BaseViewModel<RootNavigator>() {

    fun getProfiles() {

        getmNavigator()?.displayProgressBar(true)

        retrofit
            .create(ProfileService::class.java)
            .getProfileData()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnError(Consumer {
                print("Error")
            })
            ?.subscribe(Consumer
            {
                getmNavigator()?.onRecivedProfile(it)
                getmNavigator()?.displayProgressBar(false)
            })
//            .let {
//                it?.let { it1 ->
//                    compositeDisposable?.add(
//                        it1
//                    )
//                }
//            }
    }
}