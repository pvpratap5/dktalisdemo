package com.orange.dktalisapp.ui.root.DataModels


import com.google.gson.annotations.SerializedName

data class ProfileData(
    @SerializedName("results")
    val results: List<Result>
)