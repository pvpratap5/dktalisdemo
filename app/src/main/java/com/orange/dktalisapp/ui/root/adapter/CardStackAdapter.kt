package com.orange.dktalisapp.ui.root.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.orange.dktalisapp.R
import com.orange.dktalisapp.ui.root.DataModels.ProfileData
import com.orange.dktalisapp.ui.root.DataModels.Spot

class CardStackAdapter(
    private var profileDatas: ArrayList<ProfileData>
) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_propect, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val userInfo = profileDatas[position].results[0]

        holder.name.text =
            userInfo.user.name.title.capitalize() + " " + userInfo.user.name.first.capitalize() + " " + userInfo.user.name.last.capitalize()

        holder.city.text = userInfo.user.gender.capitalize()

        var stringURL = userInfo.user.picture;

        stringURL = stringURL.replace("http", "https")

        Glide.with(holder.image)
            .applyDefaultRequestOptions(RequestOptions.circleCropTransform())
            .load(stringURL)
            .into(holder.image)

        holder.itemView.setOnClickListener { v ->
            Toast.makeText(v.context, userInfo.user.username, Toast.LENGTH_SHORT).show()
        }

        holder.userProfileBtn.setOnClickListener(View.OnClickListener {
            holder.name.text = userInfo.user.name.title.capitalize() + " " + userInfo.user.name.first.capitalize() + " " + userInfo.user.name.last.capitalize()
            holder.city.text = userInfo.user.gender.capitalize()
        })

        holder.calendarBtn.setOnClickListener(View.OnClickListener {
            holder.name.text = "Calendar"
            holder.city.text = "No Details in API"
        })

        holder.locationBtn.setOnClickListener(View.OnClickListener {
            holder.name.text = "Location"
            holder.city.text =
                userInfo.user.location.city.capitalize() + "," + userInfo.user.location.state.capitalize()
        })

        holder.phoneBtn.setOnClickListener(View.OnClickListener {
            holder.name.text = "Phone Number"
            holder.city.text = userInfo.user.phone
        })

        holder.lockBtn.setOnClickListener(View.OnClickListener {
            holder.name.text = "Lock"
            holder.city.text = "No Details in API"
        })

    }

    override fun getItemCount(): Int {
        return profileDatas.size
    }

    fun setSpots(spots: ArrayList<ProfileData>) {
        this.profileDatas = spots
    }

    fun addSpots(data: ProfileData) {
        this.profileDatas.removeAll(profileDatas)
        this.profileDatas.add(data)
    }

    fun getSpots(): ArrayList<ProfileData> {
        return this.profileDatas
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.titleText)
        var city: TextView = view.findViewById(R.id.subtitleText)
        var image: ImageView = view.findViewById(R.id.userImage)
        var userProfileBtn: ImageButton = view.findViewById(R.id.button1);
        var calendarBtn: ImageButton = view.findViewById(R.id.button2);
        var locationBtn: ImageButton = view.findViewById(R.id.button3);
        var phoneBtn: ImageButton = view.findViewById(R.id.button4);
        var lockBtn: ImageButton = view.findViewById(R.id.button5);

    }

}
