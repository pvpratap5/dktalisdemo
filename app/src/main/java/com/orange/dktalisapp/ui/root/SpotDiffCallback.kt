package com.orange.dktalisapp.ui.root

import androidx.recyclerview.widget.DiffUtil
import com.orange.dktalisapp.ui.root.DataModels.ProfileData

class SpotDiffCallback(
        private val old: List<ProfileData>,
        private val new: List<ProfileData>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition].results[0].user.dob == new[newPosition].results[0].user.dob
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition] == new[newPosition]
    }

}
