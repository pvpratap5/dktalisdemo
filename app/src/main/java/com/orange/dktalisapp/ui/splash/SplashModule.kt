package com.orange.dktalisapp.ui.splash

import androidx.lifecycle.ViewModelProvider
import com.orange.dktalis.appbase.ViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class SplashModule {
    @Provides
    internal fun SplashViewModelProvider(splashViewModel: SplashViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(splashViewModel)
    }

    @Provides
    internal fun provideSplashViewModel(): SplashViewModel {
        return SplashViewModel()
    }
}