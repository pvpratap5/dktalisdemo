package com.orange.dktalisapp.ui.root.DataModels


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("seed")
    val seed: String,
    @SerializedName("user")
    val user: User,
    @SerializedName("version")
    val version: String
)