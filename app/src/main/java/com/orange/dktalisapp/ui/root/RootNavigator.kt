package com.orange.dktalisapp.ui.root

import com.orange.dktalisapp.ui.root.DataModels.ProfileData

interface RootNavigator {
    fun onRecivedProfile(profile: ProfileData)
    fun onRecivedError(error:String)
    fun displayProgressBar(flag:Boolean)
}