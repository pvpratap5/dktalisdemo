package com.orange.dktalisapp.ui.root.DataModels


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("cell")
    val cell: String,
    @SerializedName("dob")
    val dob: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("location")
    val location: Location,
    @SerializedName("md5")
    val md5: String,
    @SerializedName("name")
    val name: Name,
    @SerializedName("password")
    val password: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("picture")
    val picture: String,
    @SerializedName("registered")
    val registered: String,
    @SerializedName("SSN")
    val sSN: String,
    @SerializedName("salt")
    val salt: String,
    @SerializedName("sha1")
    val sha1: String,
    @SerializedName("sha256")
    val sha256: String,
    @SerializedName("username")
    val username: String
)