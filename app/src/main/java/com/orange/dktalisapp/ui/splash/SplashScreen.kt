package com.orange.dktalisapp.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.orange.dektalisapp.appbase.BaseActivity
import com.orange.dktalisapp.BR
import com.orange.dktalisapp.R
import com.orange.dktalisapp.databinding.ActivitySplashScreenBinding
import com.orange.dktalisapp.ui.root.RootScreen
import javax.inject.Inject

class SplashScreen : BaseActivity<ActivitySplashScreenBinding, SplashViewModel>(), SplashNavigator {

    @Inject
    internal lateinit var mSpalshViewModel: SplashViewModel

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.activity_splash_screen

    override val viewModel: SplashViewModel
        get() = mSpalshViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()

        Handler().postDelayed({
            var intent = Intent(applicationContext,RootScreen::class.java)
            startActivity(intent)
        }, 5000)
    }

}
