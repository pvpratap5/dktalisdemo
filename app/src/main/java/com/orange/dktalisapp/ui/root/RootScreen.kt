package com.orange.dktalisapp.ui.root

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.orange.dektalisapp.appbase.BaseActivity
import com.orange.dktalisapp.BR
import com.orange.dktalisapp.R
import com.orange.dktalisapp.databinding.ActivityRootScreenBinding
import com.orange.dktalisapp.ui.root.DataModels.ProfileData
import com.orange.dktalisapp.ui.root.DataModels.Spot
import com.orange.dktalisapp.ui.root.adapter.CardStackAdapter
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.activity_root_screen.*
import java.util.ArrayList
import javax.inject.Inject

class RootScreen : BaseActivity<ActivityRootScreenBinding, RootViewModel>(), RootNavigator,
    CardStackListener {

    var newProfileData: ProfileData? = null

    @Inject
    internal lateinit var mViewModel: RootViewModel

    private val manager by lazy { CardStackLayoutManager(this, this) }

    var adapter: CardStackAdapter? = null

    var activityRootScreenBinding: ActivityRootScreenBinding? = null

    var mLayoutManager: LinearLayoutManager = LinearLayoutManager(this);

    override val bindingVariable: Int
        get() = BR.viewModel

    override val viewModel: RootViewModel
        get() = mViewModel

    override val layoutId: Int
        get() = R.layout.activity_root_screen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        centerToolbarText(getString(R.string.app_name))
        mViewModel.setmNavigator(this)
        //get first profile on load
        mViewModel.getProfiles()
        //load adpater
    }


    private fun centerToolbarText(title: String) {
        val mTitleTextView = AppCompatTextView(this)
        mTitleTextView.text = title
        mTitleTextView.setSingleLine()
        mTitleTextView.textSize = 25f
        val layoutParams = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT
        )
        layoutParams.gravity = Gravity.CENTER
        supportActionBar?.setCustomView(mTitleTextView, layoutParams)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
    }


    override fun onRecivedProfile(profile: ProfileData) {
        Toast.makeText(applicationContext, "Data Recived", Toast.LENGTH_LONG).show()

        if (adapter == null) {
            var list: ArrayList<ProfileData> = ArrayList()
            list.add(profile)
            adapter = CardStackAdapter(list)
            initialize()
        } else {
            paginate(profile)
        }
    }

    override fun onRecivedError(error: String) {
    }

    fun setupCardStackView() {
        initialize()
    }

    private fun initialize() {
        manager.setStackFrom(StackFrom.None)
        manager.setVisibleCount(3)
        manager.setTranslationInterval(8.0f)
        manager.setScaleInterval(0.95f)
        manager.setSwipeThreshold(0.3f)
        manager.setMaxDegree(20.0f)
        manager.setDirections(Direction.HORIZONTAL)
        manager.setCanScrollHorizontal(true)
        manager.setCanScrollVertical(true)
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
        manager.setOverlayInterpolator(LinearInterpolator())
        cardViewStack.layoutManager = manager

        cardViewStack.adapter = adapter
        cardViewStack.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }

    override fun onCardDragging(direction: Direction, ratio: Float) {
//        Log.d("CardStackView", "onCardDragging: d = ${direction.name}, r = $ratio")
    }

    override fun onCardSwiped(direction: Direction) {
//        Log.d("CardStackView", "onCardSwiped: p = ${manager.topPosition}, d = $direction")

        mViewModel.getProfiles()
    }

    override fun onCardRewound() {
//        Log.d("CardStackView", "onCardRewound: ${manager.topPosition}")
    }

    override fun onCardCanceled() {
//        Log.d("CardStackView", "onCardCanceled: ${manager.topPosition}")
    }

    override fun onCardAppeared(view: View, position: Int) {
        val textView = view.findViewById<TextView>(R.id.item_image)
//        Log.d("CardStackView", "onCardAppeared: ($position) ${textView.text}")
    }

    override fun onCardDisappeared(view: View, position: Int) {
        val textView = view.findViewById<TextView>(R.id.item_name)
//        Log.d("CardStackView", "onCardDisappeared: ($position) ${textView.text}")
    }

    private fun paginate(profile: ProfileData) {
        val old = adapter?.getSpots()
        val callback = old?.let { SpotDiffCallback(it, listOf(profile)) }
        val result = callback?.let { DiffUtil.calculateDiff(it) }
        adapter?.addSpots(profile)
        adapter?.setSpots(adapter!!.getSpots())
        adapter?.let { result?.dispatchUpdatesTo(it) }
    }

    override fun displayProgressBar(flag: Boolean) {
        if (flag)
            progressView.visibility = View.VISIBLE
        else
            progressView.visibility = View.GONE
    }


}
