package com.orange.dktalisapp.ui.root

import androidx.lifecycle.ViewModelProvider
import com.orange.dktalis.appbase.ViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class RootModule {
    @Provides
    internal fun ViewModelProvider(rootViewModel: RootViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(rootViewModel)
    }

    @Provides
    internal fun provideRootViewModel(): RootViewModel {
        return RootViewModel()
    }
}